<?php

/**
 * The abstract base that can be used to implement an specialized User / Role / Group Access-Coordinator.
 *
 * There are four functions defined:
 *  o matchGroup: Extracts the workinggroup shortname of a rule name. The parameter
 *    $s is the title of the role, $altparams is an optional and implementation
 *    dependent parameter.
 *
 *  o hasFullAccess: Determines whether a role of a user provices privileged access.
 *    The parameter $role is the title of the role, $altparams is an optional and
 *    implementation dependent parameter.
 *
 *  o selectFullAccessGroups: Determines a list of workinggroups that a user is
 *    associated with additionally, when priviledged access is detected.
 *    $altparams is an optional and implementation dependent parameter.
 *
 *  o getUserAssociatedGroups: Determines a list of workinggroups that a user is
 *    associated with. The parameter $userroles must be a complete list of all
 *    relevant roles if the user. $altparams is an optional and implementation
 *    dependent parameter.
 *
 */
abstract class UserRoleGroupAccessCoordinator {
	abstract protected function matchGroup($s, $altparams);
	abstract protected function hasFullAccess($role, $altparams);
	abstract protected function selectFullAccessGroups($altparams);
	abstract protected function getUserAssociatedGroups($userroles, $altparams);
}

/**
 * The interface that defines the needed signature for getting the current users groups.
 *
 * There is one function defined:
 *  o getGroupsOfCurrentUser: Determines a list of workinggroups that a user is
 *    associated with.
 */
interface IGroupAccessCoordinator {
	function getGroupsOfCurrentUser();
}

abstract class GroupAccessCoordinatorBase extends UserRoleGroupAccessCoordinator implements IGroupAccessCoordinator {
	// A list of roles with all_access privileges
	public $full_access_groups;

	public function __construct($full_access_groups = []) {
		$this->full_access_groups = $full_access_groups;
	}

	/**
	 * Checks whether the specified role is in the list of full access groups.
	 *
	 * @param  string  $role      The role to check.
	 * @param  mixed   $altparams An optional and implementation specific parameter.
	 * @return boolean            Whether the specified role is in the list of full access groups or not.
	 */
	protected function hasFullAccess($role, $altparams = NULL) {
		if(in_array($role, $this->full_access_groups)) {
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * The abstract base for a method that extracts the workinggroup shortname.
	 *
	 * @param  string $s         The title of the role.
	 * @param  mixed  $altparams An optional and implementation specific parameter.
	 * @return string            The shortname of the workinggroup.
	 */
	abstract protected function extractWorkinggroupName($s, $altparams);

	/**
	 * Extracts the workinggroup shortname of a user role.
	 *
	 * @param  string $s         The user role.
	 * @param  mixed  $altparams An optional and implementation specific parameter.
	 * @return string            The shortname of the workinggroup.
	 */
	protected function matchGroup($s, $altparams = NULL) {
		return $this->extractWorkinggroupName($s, $altparams);
	}

	/**
	 * Determines a list of workinggroups that a user is associated with.
	 *
	 * @param  string $roles     The list of roles of the user.
	 * @param  mixed  $altparams An optional and implementation specific parameter.
	 * @return array             A list of the workinggroups the user is associated with.
	 */
	protected function getUserAssociatedGroups($roles, $altparams = NULL) {
		// A variable to store whether the user has full access (admin)
		$full_access = FALSE;

		// A list of matched workinggroups
		$matched_wgs = array();

		// Get associated workinggroups or detect full access privilege
		// -> Iterate over all given roles and check
		foreach ($roles as $key => $role) {
			// Check if role has full access (admin)
			if($this->hasFullAccess($role, NULL)) {
				$full_access = TRUE;
				break;
			}

			// Determine workinggroup associated to the role
			$r = $this->matchGroup($role, NULL);

			// Add the matched pattern to the list of matched workinggroups if existent
			if($r !== NULL) {
				$matched_wgs[] = $r;
			}
		}

		// Check for full access and associate all special groups
		if($full_access) {
			return $this->selectFullAccessGroups(NULL);
		}

		// Return the whole list of matched workinggroups
		return $matched_wgs;
	}

	/**
	 * Determines a list of workinggroups that a user is associated with based
	 * on the complete list of user roles of the user.
	 *
	 * @return array A list of the workinggroups the user is associated with.
	 */
	public function getGroupsOfCurrentUser() {
		// Determine the current user
		global $user;

		// The roles of the current user
		$roles = $user->roles;

		// Detect Workinggroups
		return $this->getUserAssociatedGroups($roles, NULL);
	}

	/**
	 * Determines whether the user is authorized by comparing the specified
	 * argument with the list of the user's groups.
	 *
	 * @param  string  $indata The argument to compare to.
	 * @return boolean         Whether the argument given is in the list of the user's groups.
	 */
	public function isAuthorized($indata) {
		$usergroups = $this->getGroupsOfCurrentUser();

		// Check whether the given indata is in the list of the user's groups
		return in_array($indata, $usergroups, TRUE);
	}
}

/**
 * An abstraction of the needed database queries for implementation of a specific
 * GroupAccessCoordinatorBase-derived class.
 *
 * The database operation to determine a list of groups that is associated with
 * users that are deemed highly privileged is abstracted using conditional
 * select-queries and a regular expression.
 *
 * Also the extraction of a workinggroup shortname is performed by matching a
 * regular expression against a role title.
 *
 * Both regular expressions are expected as parameter on instantiation of this
 * class. Also a list of roles can be provided, denoting highly privileged users.
 */
class DatabaseConditionalGroupAccessCoordinator extends GroupAccessCoordinatorBase {
	/**
	 * The regular expression pattern that is used to extract a shortname of a
	 * workinggroup by matching the parts of a role title.
	 *
	 * @var string
	 */
	protected $extraction_rgxp;

	/**
	 * The regular expression patten that is used to match the list of roles that
	 * contain workinggroups a highly privileged user should be associated with.
	 * @var string
	 */
	protected $db_rgxp;

	/**
	 * Instantiate a new DatabaseConditionalGroupAccessCoordinator object.
	 *
	 * @param string $extractionRGXP     The regular expression pattern that is used to extract a shortname of a workinggroup by matching the parts of a role title.
	 * @param string $DBRGXP             The regular expression patten that is used to match the list of roles that contain workinggroups a highly privileged user should be associated with.
	 * @param array  $full_access_groups A list of roles denoting highly privileged users.
	 */
	public function __construct($extractionRGXP, $DBRGXP, $full_access_groups = []) {
		parent::__construct($full_access_groups);

		$this->extraction_rgxp = $extractionRGXP;
		$this->db_rgxp = $DBRGXP;
	}

	protected function extractWorkinggroupName($s, $altparams = NULL) {
		// Match role name for working group shortname
		if(!preg_match($this->extraction_rgxp, $s, $match)) {
			return NULL;
		}

		return $match[1];
	}

	protected function selectFullAccessGroups($altparams = NULL) {
		$result = db_select('role', 'a')
		          ->fields('a', array('name'))
		          ->condition('name', $this->db_rgxp, 'REGEXP')
		          ->execute();
		$rows = $result->fetchAllAssoc('name');

		// Create a list of ALL workinggroups
		$matched_wgs = array();

		// Perform matching
		foreach ($rows as $value) {
			$r = $this->matchGroup($value->name, NULL);

			if($r !== NULL) {
				$matched_wgs[] = $r;
			}
		}

		return $matched_wgs;
	}
}

/**
 * A class that is used to decorate any IGroupAccessCoordinator by matching the
 * shortnames of a workinggroup and both, a list of well known workinggroup
 * titles, and the result of an optional decofunc-function.
 */
class WorkgroupNameDecorator implements IGroupAccessCoordinator {
	protected $decorated;
	protected $map;
	protected $df = NULL;

	public function __construct(IGroupAccessCoordinator $gac, $groupmap, $decofunc) {
		$this->decorated = $gac;
		$this->map = $groupmap;
		$this->df = $decofunc;
	}

	public function getGroupsOfCurrentUser() {
		return $this->decorated->getGroupsOfCurrentUser();
	}

	public function getGroupsOfCurrentUserMapped() {
		$user_wgs = $this->getGroupsOfCurrentUser();

		$wgs = array_combine($user_wgs, $user_wgs);

		$nameswg = $this->map;
		$df = $this->df;

		// Perform mapping
		foreach($wgs as $k => $_) {
			// Check whether a static mapping can be performed with well known entities
			if(!array_key_exists($k, $nameswg)) {
				// Check whether a dynamic mapping can be performed instead
				if($df !== NULL && is_callable($df)) {
					$t = call_user_func_array($df, [$k]);

					// Set value on success
					if($t !== NULL) {
						$wgs[$k] = $t;
					}
				}

				// Skip if there is no dynamic mapping; or if the mapping has succeeded
				continue;
			}

			$wgs[$k] = $nameswg[$k];
		}

		return $wgs;
	}

	public static function Decorate(IGroupAccessCoordinator $gac, $groupmap, $decofunc = NULL) {
		return new self($gac, $groupmap, $decofunc);
	}
}