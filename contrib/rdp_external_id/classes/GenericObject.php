<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 27.07.18
 * Time: 14:52
 */

class GenericObject implements GenericObjectInterface {

  use GenericObjectTrait;

  private static $table_name = '';

  private static $attributes = [];

  protected static function tableHeader() {
    $header = [];
    foreach (self::$attributes as $key => $values) {
      if (array_key_exists('name', $values)) {
        $header[] = $values['name'];
      }
    }
    return $header;
  }

  protected function tableRow() {
    return $this->variables;
  }

  public function displayPage($classname = self::class) {
    $list = [$this];
    return $classname::table($list);
  }

  public function getForm($form_type) {
    // TODO: Implement getForm() method.
  }

  public function getFormField($field) {
    // TODO: Implement getFormField() method.
  }
}
