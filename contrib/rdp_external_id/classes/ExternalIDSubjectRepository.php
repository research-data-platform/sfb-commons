<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 30.07.18
 * Time: 11:58
 */

abstract class ExternalIDSubjectRepository extends GenericRepository {

  public static function findById($id, $class = NULL) {
    $class = ExternalIDSubject::class;
    return parent::findById($id, $class);
  }

  public static function findAll($class = NULL) {
    $class = ExternalIDSubject::class;
    return parent::findAll($class);
  }

  public static function findBySubject($subject) {
    $result = parent::findByConditions(ExternalIDSubject::class,
      ['subject' => $subject]);
    if (count($result) == 1) {
      return array_pop($result);
    }
    else {
      return $result;
    }
  }
}
