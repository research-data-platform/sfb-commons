<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 29.07.18
 * Time: 09:44
 */

class GenericObjectForm {

  /**
   * @var \GenericObjectInterface $object
   */
  private $object = NULL;

  public function __construct(GenericObjectInterface $object) {
    $this->object = $object;
  }

  private static function build_options_array($class) {
    $objects = GenericRepository::findAll($class);
    $options = [];
    foreach ($objects as $object) {
      $label = $class == ExternalIDSubject::class ? $object->getSubject() : $object->getLabel();
      $options[$object->getId()] = $label;
    }
    return $options;
  }

  public function getForm($form_type = 'create') {
    $attributes = get_class($this->object)::getAttributes();

    $form = [];

    if ($form_type == 'edit') {
      $form['id'] = [
        '#type' => 'hidden',
        '#value' => $this->object->getId(),
        '#access' => FALSE,
      ];
    }

    foreach ($attributes as $key => $spec) {
      $form[$key] = self::getFormField($key, $spec);
    }

    $form['class'] = [
      '#type' => 'hidden',
      '#value' => get_class($this->object),
      '#access' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'save',
    ];

    return $form;
  }

  private function getFormField($fieldname, $specification) {
    $variables = $this->object->getVars();
    $form_field = [];

    switch ($specification['class']) {
      case ExternalIDSubject::class:
      case ExternalIDType::class:
        $form_field['#options'] = self::build_options_array($specification['class']);
        $type = 'select';
        break;
      case 'text':
        $type = 'textarea';
        break;
      case 'bool':
        $type = 'checkbox';
        break;
      case 'integer':
      case 'string':
      default:
        $type = 'textfield';
        break;
    }
    $form_field['#type'] = $type;

    $form_field['#title'] = t($specification['name']);

    if (array_key_exists($fieldname, $variables)) {
      $form_field['#default_value'] = $variables[$fieldname];
    }
    elseif (array_key_exists('default_value', $specification)) {
      $form_field['#default_value'] = $specification['default_value'];
    }

    if (array_key_exists('attributes', $specification)) {
      $form_field = array_merge($form_field, $specification['attributes']);
    }

    /**
     * Checks basic Drupal From API control verbs,
     * see the list at https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7.x
     */
    $drupal_form_controls_basic = [
      'description',
      'required',
      'size',
      'maxlength',
    ];
    foreach ($drupal_form_controls_basic as $control) {
      if (array_key_exists($control, $specification)) {
        $form_field['#' . $control] = t($specification[$control]);
      }
    }

    return $form_field;
  }

  public static function validate($class, $values) {
    $attributes = $class::getAttributes();

    foreach ($values as $key => $value) {
      if (array_key_exists($key, $attributes)) {
        // ToDo: validation...
      }
    }
  }
}