<?php
/**
 * @file Form and action handler to add working group.
 *
 * @author Markus Suhr (markus.suhr@med.uni-goettingen.de)
 */

/**
 * Form to add new working group.
 *
 * @return array Drupal form.
 */
function sfb_commons_config_groups_add(){
  $form = [];

  $working_group = new WorkingGroup();

  $form['short_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Short name'),
    '#description' => t('Research group short name'),
    '#default_value' => $working_group->getShortName(),
    '#required' => true,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Research group name'),
    '#default_value' => $working_group->getName(),
    '#required' => true,
  );

  $form['leader'] = array(
    '#type' => 'textfield',
    '#title' => t('Leader'),
    '#description' => t('Leader'),
    '#default_value' => $working_group->getLeader(),
    '#required' => true,
  );

  $form['leader_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Leader\'s E-mail'),
    '#description' => t('E-mail address'),
    '#default_value' => $working_group->getLeaderEmail(),
  );

  $form['leader_institution'] = array(
    '#type' => 'textfield',
    '#title' => t('Leader\'s institution'),
    '#description' => t('Institution'),
    '#default_value' => $working_group->getLeaderInstitution(),
    '#required' => true,
  );

  $form['leader_institution_ror_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Leader\'s institutions ROR ID'),
    '#description' => t('Institutions ROR ID'),
    '#default_value' => $working_group->getLeaderInstitution(),
  );

  $form['leader_orcid'] = array(
    '#type' => 'textfield',
    '#title' => t('Leader\'s ORCID'),
    '#description' => t('ORCID'),
    '#default_value' => $working_group->getLeaderORCID(),
    '#required' => true,
  );

  // submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}
/**
 * Validation for group add form.
 */
function sfb_commons_config_groups_add_validate($form, &$form_state){

    /** format validation */
    if(!WorkingGroup::validateORCID($form_state['values']['leader_orcid']))
        form_set_error('leader_orcid', t('Give a valid ORCID (e.g. 0000-0000-0000-0000) or leave field with \'unknown\'.'));

    if(!WorkingGroup::validateRorId($form_state['values']['leader_institution_ror_id']))
        form_set_error('leader_institution_ror_id', t('Give a valid ROR ID (e.g. 021ft0n22) or leave field with \'unknown\'.'));
}

/**
 * Handle sfb_commons_config_groups_add submit action.
 * Translate form field into WorkingGroup object and save it to database.
 *
 * @param $form
 * @param $form_state
 */
function sfb_commons_config_groups_add_submit($form, &$form_state) {

  $working_group = new WorkingGroup();

  // set class members with data from the form
  $working_group->setShortName($form_state['values']['short_name']);
  $working_group->setName($form_state['values']['name']);
  $working_group->setLeader($form_state['values']['leader']);
  $working_group->setLeaderEmail($form_state['values']['leader_email']);
  $working_group->setLeaderInstitution($form_state['values']['leader_institution']);
  $working_group->setLeaderInstitutionRorId($form_state['values']['leader_institution_ror_id']);
  $working_group->setLeaderORCID($form_state['values']['leader_orcid']);

  // store the data into database
  if(!$working_group->save()) {
    drupal_set_message(t('There was a problem with the database. Form could not be stored!'), 'error');
    return;
  }

  // redirect to working groups overview
  $form_state['redirect'] = SFB_COMMONS_URL_CONFIG_WORKINGGROUPS;
  return;
}
