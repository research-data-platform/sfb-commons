<?php

/**
 * @file
 * Lists all working groups
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 * @author Markus Suhr (markus.suhr@med.uni-goettingen.de)
 */

/**
 * @return string Drupal page content.
 */
function sfb_commons_config_groups() {

  //html button for creating new research group
  //$btn_create_group = '<a href="'. SFB_COMMONS_URL_CONFIG_WORKINGGROUPS_ADD.'" type="button" class="btn btn-success btn-xs"><span class="icon glyphicon glyphicon-plus"></span> Add new research group (not yet implemented)</a>';

  // Array used for table header
  $header = array(
    array('data' => t('ID'), 'field' => 'id'),
    array('data' => t('Short name'), 'field' => 'short_name'),
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Leader'), 'field' => 'leader'),
    array('data' => t('Leader email'), 'field' => 'leader_email'),
    array('data' => t('Leader institution'), 'field' => 'leader_institution'),
    array('data' => t('Leader institution ROR ID'), 'field' => 'leader_institution_ror_id'),
    array('data' => t('Leader ORCID'), 'field' => 'leader_orcid'),
    array('data' => t('Logo')),
    array('data' => t('Operations')),
  );

  //TODO: implement sorting functions

  // variable used for table rows
  $rows = array();

  // fill rows with working groups data
  $working_groups = WorkingGroupRepository::findAll(array('extend' => 'TableSort', 'orderByHeader' => $header));
  foreach($working_groups as $working_group) {
    $rows[] = array(
      $working_group->getId(),
      $working_group->getShortName(). (!$working_group->roleExists() ? ' <strong style="color: red;">role doesnt\'t exists</strong>' : ''),
      $working_group->getName(),
      $working_group->getLeader(),
      $working_group->getLeaderEmail(),
      $working_group->getLeaderInstitution(),
      $working_group->getLeaderInstitutionRorId(),
      $working_group->getLeaderORCID(),
      '<img src="' . variable_get(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH) . $working_group->getShortName() . '20x20.png" alt="missing" />',
      '<a href="'.sfb_commons_url(SFB_COMMONS_URL_CONFIG_WORKINGGROUPS_EDIT, $working_group->getId()).'">edit</a>',
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}