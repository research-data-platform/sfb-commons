<?php

/**
 * @file
 * A place for commonly used functions and classes.
 * 
 * @author
 * Alexander Wildschütz - alexander.wildschuetz@gwdg.de
 */

/**
 * Strips several accents and replaces german umlaute with their transliteration.
 * 
 * @param string $str The string to sanify
 * @return string String without several accented letters
 */
function unidecode($str, $lower=FALSE) {
	// Replace german umlaute with their transliteration
	$umlaute = array("'ä'", "'ö'", "'ü'", "'Ä'", "'Ö'", "'Ü'", "'ß'");
	$replace = array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss');

	$str = preg_replace($umlaute, $replace, $str);

	// Transform accented characters to their basic-latin representation
	$search = 'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ';
	$ansi   = 'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy';

	if($lower) {
		$str = strtolower($str);
		$ansi = 'sozsozyyuaaaaaaaceeeeiiiidnoooooouuuuysaaaaaaaceeeeiiiionoooooouuuuyy';
	}

	return utf8_encode(strtr(utf8_decode($str), utf8_decode($search), $ansi));

	// More robust approach:
	// Strip combining diacritic marks after transforming to normal form 
	// by canonical decomposition and transforming back to canonical 
	// composition afterwards
//	$transliterator = Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: NFC;', Transliterator::FORWARD);
//	return $transliterator.transliterate($str);
}

/**
 * Checks if a string is NULL or whitespace only.
 *
 * @param string $str The string to check.
 *
 * @return bool TRUE if string is NULL or whitespace only, FALSE otherwise.
 */
function IsNullOrWhitespace($str) {
    return (!isset($str) || strcmp('', trim($str)) == 0);
}

function StringOrDefault($str, $default) {
	return IsNullOrWhitespace($str) ? $default : $str;
}

/**
 * Converts all functional HTML-literals to their escaped representation. 
 * This function can be used to safely output text in a HTML context.
 *
 * @param string $str The string to be converted.
 *
 * @return string The converted string, where all functional HTML-literals are escaped.
 */
function StringEscapeHtml($str) {
	return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}