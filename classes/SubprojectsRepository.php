<?php

class SubprojectsRepository {

  private static $tableName = 'subprojects';

  /**
   * Store Subproject in database (insert or update).
   *
   * @param \Subproject $subproject
   *
   * @return bool|int Returns id if inserted, true if updated, false on failure.
   */
  public static function save(Subproject $subproject) {
    // Insert, if subproject was previously empty, update row otherwise
    try {
      if ($subproject->isEmpty()) {
        $id = db_insert(self::$tableName)
          ->fields([
            'abbreviation' => $subproject->getAbbreviation(),
            'status' => $subproject->getStatus(),
            'title' => $subproject->getTitle(),
          ])
          ->execute();

        return (int) $id;
      }
      else {
        db_update(self::$tableName)
          ->fields([
            'abbreviation' => $subproject->getAbbreviation(),
            'status' => $subproject->getStatus(),
            'title' => $subproject->getTitle(),
          ])
          ->condition('id', $subproject->getId(), '=')
          ->execute();

        return TRUE;
      }
    } catch (Exception $e) {
      watchdog_exception('sfb-commons', $e);

      return FALSE;
    }
  }

  /**
   * @param $results
   *
   * @return \Subproject
   */
  public static function databaseResultsToSubproject($results) {
    $subproject = new Subproject();

    if (empty($results)) {
      return $subproject;
    }

    $subproject->setId($results->id);
    $subproject->setAbbreviation($results->abbreviation);
    $subproject->setStatus($results->status);
    $subproject->setTitle($results->title);

    return $subproject;
  }

  /**
   * @param $results
   *
   * @return Subproject[]
   */
  public static function databaseResultsToSubprojects($results) {
    $subprojects = [];

    foreach ($results as $result) {
      $subprojects[] = SubprojectsRepository::databaseResultsToSubproject($result);
    }

    return $subprojects;
  }


  /**
   * @return \Subproject[]
   */
  public static function findAll() {
    $result = db_select(self::$tableName, 's')
      ->fields('s', Subproject::$databaseFields)
      ->execute();

    return self::databaseResultsToSubprojects($result);
  }


  /**
   * @param $id
   *
   * @return \Subproject
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 's')
      ->fields('s', Subproject::$databaseFields)
      ->condition('id', $id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToSubproject($result);
  }


}