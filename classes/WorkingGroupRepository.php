<?php

class WorkingGroupRepository {

  private static $tableName = 'working_group';

  /**
   * @param $result
   *
   * @return WorkingGroup
   */
  public static function databaseResultsToWorkingGroup($result) {
    $group = new WorkingGroup();

    if (empty($result)) {
      return $group;
    }

    $group->setId($result->id);
    $group->setShortName($result->short_name);
    $group->setName($result->name);
    $group->setLeader($result->leader);
    $group->setLeaderEmail($result->leader_email);
    $group->setLeaderInstitution($result->leader_institution);
    $group->setLeaderInstitutionRorId($result->leader_institution_ror_id);
    $group->setLeaderORCID($result->leader_orcid);

    return $group;
  }

  /**
   * @param $results
   *
   * @return WorkingGroup[]
   */
  public static function databaseResultsToWorkingGroups($results) {
    $groups = [];

    foreach ($results as $result) {
      $groups[] = self::databaseResultsToWorkingGroup($result);
    }

    return $groups;
  }

  /**
   * @param $workingGroup \WorkingGroup
   *
   * @return bool
   */
  public static function save($workingGroup) {

    // if this working group is not stored in database, insert new, update row otherwise
    if ($workingGroup->isEmpty()) {

      $id = db_insert(self::$tableName)
        ->fields([
          'short_name' => $workingGroup->getShortName(),
          'name' => $workingGroup->getName(),
          'leader' => $workingGroup->getLeader(),
          'leader_email' => $workingGroup->getLeaderEmail(),
          'leader_institution' => $workingGroup->getLeaderInstitution(),
          'leader_institution_ror_id' => $workingGroup->getLeaderInstitutionRorId(),
          'leader_orcid' => $workingGroup->getLeaderORCID()
        ])
        ->execute();

      return (int) $id;

    }
    else {

      // insert or update database data
      db_merge(self::$tableName)
        ->key(['id' => $workingGroup->getId()])
        ->fields([
            'id' => $workingGroup->getId(),
            'short_name' => $workingGroup->getShortName(),
            'name' => $workingGroup->getName(),
            'leader' => $workingGroup->getLeader(),
            'leader_email' => $workingGroup->getLeaderEmail(),
            'leader_institution' => $workingGroup->getLeaderInstitution(),
            'leader_institution_ror_id' => $workingGroup->getLeaderInstitutionRorId(),
            'leader_orcid' => $workingGroup->getLeaderORCID()
          ]
        )->execute();

      return TRUE;
    }
  }

  /**
   * @param $id
   *
   * @return \WorkingGroup
   */
  public
  static function findById($id) {
    $result = db_select('working_group', 'g')
      ->condition('g.id', $id, '=')
      ->fields('g', WorkingGroup::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return WorkingGroupRepository::databaseResultsToWorkingGroup($result);
  }

  /**
   * @param $shortname
   *
   * @return \WorkingGroup
   */
  public
  static function findByShortName($shortname) {
    $result = db_select('working_group', 'g')
      ->condition('g.short_name', $shortname, '=')
      ->fields('g', WorkingGroup::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return WorkingGroupRepository::databaseResultsToWorkingGroup($result);
  }

  /**
   * @return \WorkingGroup[]
   */
  public
  static function findAll() {
    $result = db_select('working_group', 'g')
      ->fields('g', WorkingGroup::$databaseFields)
      ->orderBy('g.short_name')
      ->execute();

    return WorkingGroupRepository::databaseResultsToWorkingGroups($result);
  }
}