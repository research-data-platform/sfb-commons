<?php
/** @file ToDo: file description & doc
 */

abstract class SharingLevel {

  const PRIVATE_LEVEL = 10;

  const GROUP_LEVEL = 20;

  const SITE_LEVEL = 30;

  const PUBLIC_LEVEL = 90;

  private static function getAsArray() {
    return [
      self::PRIVATE_LEVEL => 'Private',
      self::GROUP_LEVEL => 'Group',
      self::SITE_LEVEL => 'Site',
      self::PUBLIC_LEVEL => 'Public',
    ];
  }

  private static $tooltips = [
    self::PRIVATE_LEVEL => 'Private: Only the owner of an object can see it.',
    self::GROUP_LEVEL => 'Group: Only members of the same group as the creator can see this object.',
    self::SITE_LEVEL => 'Site: All users of the site are allowed to see this object.',
    self::PUBLIC_LEVEL => 'Public: Everyone is allowed to see this object.',
  ];

  private static function getGlyphicon($sharing_level) {
    $ressources_path = drupal_get_path('module', 'sfb_commons') . '/resources';

    switch ($sharing_level) {
      case self::PRIVATE_LEVEL:
        return '<span class="glyphicon glyphicon-lock"></span>';
        break;
      case self::GROUP_LEVEL:
        $icon_path = url($ressources_path . '/glyphicon_group_h16px.png');
        break;
      case self::SITE_LEVEL:
        $icon_path = url($ressources_path . '/glyphicon_org_h16px.png');
        break;
      case self::PUBLIC_LEVEL:
        $icon_path = url($ressources_path . '/glyphicon_lock_open_h16px.png');
        break;
      default:
        return '';
    }

    return '<img src="' . $icon_path . '" style="height: 12px; vertical-align: inherit;">';
  }

  public static function isSharingLevel($sharing_level) {
    return array_key_exists($sharing_level, self::getAsArray());
  }


  public static function infoText() {
    $style = 'width: 20px; float: left; vertical-align: middle; text-align: center;';
    $text = '<p><small><div style="' . $style . '">' . self::getGlyphicon(self::PRIVATE_LEVEL) .
      '</div>&nbsp;<strong>Private</strong>: Only the owner of an object can see it.
                <br><div style="' . $style . '">' . self::getGlyphicon(self::GROUP_LEVEL) .
      '</div>&nbsp;<strong>Group</strong>: Only members of the same group as the creator can see this object. 
                <br><div style="' . $style . '">' . self::getGlyphicon(self::SITE_LEVEL) .
      '</div>&nbsp;<strong>Site</strong>: All users of the site are allowed to see this object. 
                <br><div style="' . $style . '">' . self::getGlyphicon(self::PUBLIC_LEVEL) .
      '</div>&nbsp;<strong>Public</strong>: Everyone is allowed to see this object.</small></p>';
    return $text;
  }

  public static function getTooltipText($sharing_level) {

    return self::$tooltips[$sharing_level];
  }

  /**
   * Returns the human readable name of SharingLevel.
   *
   * @param $sharing_level
   * @param bool $toLower
   *
   * @return string
   */
  public static function getName($sharing_level, $toLower = FALSE) {
    $sharing_level_name = '';

    switch ($sharing_level) {
      case self::PRIVATE_LEVEL:
        $sharing_level_name = 'Private';
        break;
      case self::GROUP_LEVEL:
        $sharing_level_name = 'Group';
        break;
      case self::SITE_LEVEL:
        $sharing_level_name = 'Site';
        break;
      case self::PUBLIC_LEVEL:
        $sharing_level_name = 'Public';
        break;
      default:
        $sharing_level_name = 'Unknown';
    }

    if ($toLower) {
      return strtolower($sharing_level_name);
    }

    return $sharing_level_name;
  }

  public static function getFormField() {
    $sharing_levels = self::getAsArray();
    return [
      '#type' => 'select',
      '#title' => t('Sharing Level'),
      '#description' => 'Select visibility level.',
      '#suffix' => self::infoText(),
      '#default_value' => self::GROUP_LEVEL,
      '#required' => TRUE,
      '#options' => $sharing_levels,
    ];
  }

  public static function getHtmlInline($sharing_level) {
    return '<div style="width: 24px; float: left; text-align: center">
        <span data-toggle="tooltip" title="'
      . self::getTooltipText($sharing_level) . '">'
      . self::getGlyphicon($sharing_level) . '</div>&nbsp;'
      . self::getName($sharing_level) . '</span>';
  }
}