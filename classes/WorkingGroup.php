<?php

class WorkingGroup {

  static $databaseFields = [
    'id',
    'short_name',
    'name',
    'leader',
    'leader_email',
    'leader_institution',
    'leader_institution_ror_id',
    'leader_orcid'
  ];

  /**
   * default id value for empty or new working group
   */
  const EMPTY_GROUP_ID = -1;

  const ICON_SIZE_SMALL = '20x20';

  const ICON_SIZE_MEDIUM = '48x48';

  const ICON_SIZE_LARGE = '64x64';

  /**
   * Working group database id
   *
   * @var int
   */
  private $id = WorkingGroup::EMPTY_GROUP_ID;

  /**
   * Short name of working group, e.g. ag_mueller
   *
   * @var string
   */
  private $shortName;

  /**
   * Name of the working group, e.g. Mueller
   *
   * @var string
   */
  private $name;

  /**
   * Working group's leader name, e.g. Jan Mueller
   *
   * @var string
   */
  private $leader;

  /**
   * Leader's e-mail address
   *
   * @var string
   */
  private $leader_email;

  /**
   * Working group's institution
   *
   * @var string
   */
  private $leader_institution;

  /**
   * Working group's institutions ROR ID
   *
   * @var string
   */
  private $leader_institution_ror_id = 'unknown';

  /**
   * Leader's ORCID
   *
   * @var string
   */
  private $leader_orcid = 'unknown';

  /**
   * @return string
   */
  public function getLeader() {
    return $this->leader;
  }

  /**
   * @param string $leader
   */
  public function setLeader($leader) {
    $this->leader = $leader;
  }

  /**
   * @return mixed
   */
  public function getLeaderEmail() {
    return $this->leader_email;
  }

  /**
   * @param mixed $leader_email
   */
  public function setLeaderEmail($leader_email) {
    $this->leader_email = $leader_email;
  }

  /**
   * @return mixed
   */
  public function getLeaderInstitution() {
    return $this->leader_institution;
  }

  /**
   * @param mixed $leader_institution
   */
  public function setLeaderInstitution($leader_institution) {
    $this->leader_institution = $leader_institution;
  }

  /**
   * @return mixed
   */
  public function getLeaderORCID(){
      return $this->leader_orcid;
  }

  /**
   * @param mixed $leader_orcid
   */
  public function setLeaderORCID($leader_orcid) {
     $this->leader_orcid = $leader_orcid;
  }

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getShortName() {
    return $this->shortName;
  }

  /**
   * @param mixed $shortName
   */
  public function setShortName($shortName) {
    $this->shortName = $shortName;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getLeaderInstitutionRorId()
  {
    return $this->leader_institution_ror_id;
  }

  /**
   * @param string $leader_institution_ror_id
   */
  public function setLeaderInstitutionRorId($leader_institution_ror_id)
  {
    $this->leader_institution_ror_id = $leader_institution_ror_id;
  }

  public function isEmpty() {
    if ($this->id == WorkingGroup::EMPTY_GROUP_ID) {
      return TRUE;
    }
    return FALSE;
  }

  public static function getLogoSizes() {
    return [
      self::ICON_SIZE_SMALL,
      self::ICON_SIZE_MEDIUM,
      self::ICON_SIZE_LARGE,
    ];
  }

  /**
   * Get path to a WorkingGroups' display icon in one of the defined image
   * resolutions.
   *
   * @param string $icon_size
   *
   * @return string
   */
  public function getIconPath($icon_size = self::ICON_SIZE_SMALL) {
    $path_relative = variable_get(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH) . $this->shortName . $icon_size . '.png';
    $path_absolute = $_SERVER['DOCUMENT_ROOT'] . $path_relative;
    if (file_exists($path_absolute)) {
      return $path_relative;
    }
    else {
      return base_path() . drupal_get_path('module', 'sfb_commons') . '/resources/ag_unknown20x20.png';
    }
  }

  /**
   * Get path to a WorkingGroups' leader icon.
   *
   * @return string
   */
  public function getLeaderIconPath() {
    $path_relative = variable_get(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH) . 'pi_' . $this->shortName . '.png';
    $path_absolute = $_SERVER['DOCUMENT_ROOT'] . $path_relative;
    if (file_exists($path_absolute)) {
      return $path_relative;
    }
    else {
      return base_path() . drupal_get_path('module', 'sfb_commons') . '/resources/ag_unknown64x64.png';
    }
  }

  /**
   * Get path to a WorkingGroups' display icon in one of the defined image
   * resolutions.
   *
   * @param string $wg_shortname
   * @param string $icon_size
   *
   * @return string
   */
  public static function getGroupIconPath($wg_shortname = 'ag_unknown', $icon_size = self::ICON_SIZE_SMALL) {

    // check for correct icon size
    if (!in_array($icon_size, self::getLogoSizes())) {
      $icon_size = self::ICON_SIZE_SMALL;
    }

    // get WorkingGroup object, if shortname exists
    if ($wg_shortname !== 'ag_unknown') {
      $working_group = WorkingGroupRepository::findByShortName($wg_shortname);
    }
    else {
      $working_group = new WorkingGroup();
    }

    // deliver icon through non-static method
    return $working_group->getIconPath($icon_size);
  }

  /**
   * Get path to a WorkingGroups' leader icon.
   *
   * @param string $wg_shortname
   *
   * @return string
   */
  public static function getLeaderIconPathByGroupName($wg_shortname = 'ag_unknown') {    // get WorkingGroup object, if shortname exists
    // get WorkingGroup object, if shortname exists
    if ($wg_shortname !== 'ag_unknown') {
      $working_group = WorkingGroupRepository::findByShortName($wg_shortname);
    }
    else {
      $working_group = new WorkingGroup();
    }

    // deliver image through non-static method
    return $working_group->getLeaderIconPath();
  }

  /**
   * Create RDP modules' user roles for a WorkingGroup
   * Works only for modules that implement the custom RDP hook @see
   * hook_rdp_roles_permissions() and the Drupal hook @see hook_permission()
   */
  function createRoles() {

    // Only modules implementing RDP-specific hook
    foreach (module_implements('rdp_roles_permissions') as $module) {

      // hook_rdp_roles_permissions() returns an array of roles and the corresponding permissions
      $roles = module_invoke($module, 'rdp_roles_permissions');

      foreach ($roles as $role => $permissions) {
        // Save new role (if not existing)
        $obj = new stdClass();
        $obj->name = $role . '-' . $this->shortName;
        if (!user_role_load_by_name($obj->name)) {
          user_role_save($obj);
        }

        // Create permissions
        $role_object = user_role_load_by_name($obj->name);
        user_role_grant_permissions($role_object->rid, $permissions);
      }
    }

  }

  /**
   * Stores this object into database
   *
   * @return bool
   */
  public function save() {
    $id = WorkingGroupRepository::save($this);

    // automatically create RDP roles for the new WorkingGroup
    $this->createRoles();

    if ($this->isEmpty() && $id) {
      $this->id = $id;
      return TRUE;
    }
    return $id;
  }

  /**
   * Check if any Drupal user roles exist for the given WorkingGroup
   *
   * @return bool
   */
  public function roleExists() {
    foreach (user_roles() as $role) {
      // Find shortName in Drupal role's machine name
      if (strpos($role, $this->getShortName()) !== FALSE) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @param string $orcid
   * @return bool
   */
  public static function validateORCID($orcid){
      return (
          preg_match( "/[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9a-zA-Z]{4}$/", $orcid ) ||
          ($orcid == 'unknown')
      );
  }

  /**
   * @param string $rorid
   * @return bool
   */
  public static function validateRorId($rorid){
      return (
          preg_match( "/[0-9a-zA-Z]{9}$/", $rorid ) ||
          ($rorid == 'unknown')
      );
  }
}